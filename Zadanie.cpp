#include <iostream>
#include <mpi.h>
#include <ctime>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>

using namespace std;

void vector_length(vector<float> tab, vector<float> &results, int numberOfVectorsPerProcess, long long int start)
{
    float sum = 0;
    for (int i = start * 3; i < (numberOfVectorsPerProcess + start) * 3; i += 3)
    {
        sum += sqrt((tab[i] * tab[i]) + (tab[i + 1] * tab[i + 1]) + (tab[i + 2] * tab[i + 2]));
        //results[i/3] = sqrt((tab[i]*tab[i])+(tab[i+1]*tab[i+1])+(tab[i+2]*tab[i+2]));
    }
    results[0] = sum;
    // if (rank < remainder){
    //     results.push_back((tab[numberOfVectorsPerProcess * size + rank + 3])*(tab[numberOfVectorsPerProcess * size + rank + 3]) +
    //     (tab[numberOfVectorsPerProcess * size + rank + 3] + 1)*(tab[numberOfVectorsPerProcess * size + rank + 3]+1) +
    //     ((tab[numberOfVectorsPerProcess * size + rank + 3] + 2)*(tab[numberOfVectorsPerProcess * size + rank + 3]+2)));
    // }
}

void vector_average(vector<float> tab, vector<float> &results, int numberOfVectorsPerProcess, long long int start)
{
    float x = 0, y = 0, z = 0;
    for (int i = start * 3; i < (numberOfVectorsPerProcess + start) * 3; i += 3)
    {
        x += tab[i];
        y += tab[i + 1];
        z += tab[i + 2];
    }
    results[0] = x;
    results[1] = y;
    results[2] = z;
}

void vector_length2(vector<float> tab, vector<float> &results, int numberOfVectorsPerProcess)
{
    float sum = 0;
    for (int i = 0; i < (numberOfVectorsPerProcess)*3; i += 3)
    {
        sum += sqrt((tab[i] * tab[i]) + (tab[i + 1] * tab[i + 1]) + (tab[i + 2] * tab[i + 2]));
    }
    results[0] = sum;
}

void vector_average2(vector<float> tab, vector<float> &results, int numberOfVectorsPerProcess)
{
    float x = 0, y = 0, z = 0;
    for (int i = 0; i < (numberOfVectorsPerProcess)*3; i += 3)
    {
        x += tab[i];
        y += tab[i + 1];
        z += tab[i + 2];
    }
    results[0] = x;
    results[1] = y;
    results[2] = z;
}

int main(int argc, char *argv[])
{
    MPI::Init(argc, argv);

    int size = MPI::COMM_WORLD.Get_size(); //number of processes
    int rank = MPI::COMM_WORLD.Get_rank();
    bool mode2 = false;

    string line;

    if (argc != 3)
    {
        MPI::Finalize();
        return 0;
    }
    if (string(argv[2]) == "mode2")
    {
        mode2 = true;
    }
    else if (string(argv[2]) == "mode1")
    {
        mode2 = false;
     }

    ifstream infile;
    MPI::File infile2;
    long long int fileSize = 0;
    if (mode2 == false)
    {
        infile.open(argv[1]);
        infile.seekg(0, ios::end);
        fileSize = infile.tellg();
        infile.seekg(0, ios::beg);
        infile.clear();
    }
    else
    {
        infile2 = MPI::File::Open(MPI::COMM_WORLD, argv[1], MPI_MODE_RDONLY, MPI_INFO_NULL);
        fileSize = infile2.Get_size();
    }

    long long int linesCount = fileSize / 40; //number of lines (vectors)
    int numberOfVectorsPerProcess = linesCount / size;
    long long int rest = linesCount % size;

    long long int start = rank * numberOfVectorsPerProcess + rest;

    if (rank < rest)
    {
        numberOfVectorsPerProcess += 1;
        start = rank * numberOfVectorsPerProcess;
    }

    vector<float> tab;
    if (mode2 == false)
    {
        tab.resize(linesCount * 3);
    }
    else
    {
        tab.resize(numberOfVectorsPerProcess * 3);
    }

    vector<float> results(1);
    vector<float> results2(3);

    double times[3] = {0};

    long long int counter = 0;

    times[0] = MPI::Wtime();
    if (mode2 == false)
    {
        while (getline(infile, line))
        {
            istringstream iss(line);
            float x, y, z;
            iss >> x >> y >> z;
            tab[counter] = x;
            tab[counter + 1] = y;
            tab[counter + 2] = z;
            counter += 3;
        }
        infile.close();
    }
    else
    {
        char *buffer = new char[40 * numberOfVectorsPerProcess];

        infile2.Read_at(start * 40, buffer, 40 * numberOfVectorsPerProcess, MPI::CHAR);
        infile2.Close();

        for (int i = 0; i < numberOfVectorsPerProcess; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                tab[i * 3 + j] = atof(buffer + (i * 40) + (j * 13));
            }
        }
        delete[] buffer;
    }

    times[0] = MPI::Wtime() - times[0];

    times[1] = MPI::Wtime();
    if (mode2 == false)
    {
        vector_length(tab, results, numberOfVectorsPerProcess, start);
        vector_average(tab, results2, numberOfVectorsPerProcess, start);
    }
    else
    {
        vector_length2(tab, results, numberOfVectorsPerProcess);
        vector_average2(tab, results2, numberOfVectorsPerProcess);
    }
    times[1] = MPI::Wtime() - times[1];

    float *resultsOut;

    if (rank == 0)
    {
        resultsOut = new float[4];
    }

    results2.push_back(results[0]);
    times[2] = MPI::Wtime();
    MPI::COMM_WORLD.Reduce(&results2[0], resultsOut, 4, MPI::FLOAT, MPI::SUM, 0);
    
    times[2] = MPI::Wtime() - times[2];

    double *timesOut;

    if (rank == 0)
    {
        printf("%f %f %f %f\n", resultsOut[0] / linesCount, resultsOut[1] / linesCount, resultsOut[2] / linesCount, resultsOut[3] / linesCount);
        delete[] resultsOut;
        timesOut = new double[size * 3];
    }
    MPI::COMM_WORLD.Gather(times, 3, MPI::DOUBLE, timesOut, 3, MPI::DOUBLE, 0);

    if (rank == 0)
    {
        double total = 0, readData = 0, processData = 0, reduceResults = 0;
        ofstream file("timesFile.txt");
        for (int i = 0; i < size * 3; i += 3)
        {
            printf("%lf %lf %lf\n", timesOut[i], timesOut[i + 1], timesOut[i + 2]);
            file << "Times from process %i: \n" << rank;
            file << timesOut[i] << " " << timesOut[i + 1] << " " << timesOut[i + 2] << " " << timesOut[i] + timesOut[i + 1] + timesOut[i + 2] << endl;
            readData += timesOut[i];
            processData += timesOut[i + 1];
            reduceResults += timesOut[i + 2];
            total += timesOut[i] + timesOut[i + 1] + timesOut[i + 2];
        }
        delete[] timesOut;

        file << "\nTotal timings: \n"
             << "readData: " << readData << "\nprocessData: " << processData << "\nreduceResults: " << reduceResults << "\ntotal: " << total / size << endl;
        file.close();
    }

    MPI::Finalize();
    return 0;
}